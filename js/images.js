import imageOne from '../img/01.jpg'
import imageTwo from '../img/02.jpg'
import imageThree from '../img/03.jpg'
import imageFour from '../img/04.jpg'

const images = {
  imageOne,
  imageTwo,
  imageThree,
  imageFour
}

export default images